FROM ubuntu

RUN apt-get update -y && apt-get install -y python-pip python-pytest

RUN pip install -U selenium
RUN pip install xmlrunner

