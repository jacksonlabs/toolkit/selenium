#!/bin/bash

docker-compose up -d driver

rm -f results/results.xml

docker-compose run --rm tester py.test --junitxml /root/results/results.xml /root/tests/test.py

docker-compose stop driver

docker-compose rm -f
