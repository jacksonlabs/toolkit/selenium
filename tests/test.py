import unittest
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class Test1(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Remote(command_executor='http://driver:4444/wd/hub', desired_capabilities=DesiredCapabilities.CHROME)

    def test_search_in_python_org(self):
        driver = self.driver

        driver.get('http://thedemosite.co.uk/savedata.php')

        username = driver.find_element_by_name('username')
        username.send_keys("admin")

        password = driver.find_element_by_name('password')
        password.send_keys("admin")

        submit_button = driver.find_element_by_name('FormsButton2')
        submit_button.click()

        driver.get('http://thedemosite.co.uk/login.php')

        username = driver.find_element_by_name('username')
        username.send_keys("admin")

        password = driver.find_element_by_name('password')
        password.send_keys("admin")

        submit_button = driver.find_element_by_name('FormsButton2')
        submit_button.click()

        assert "Successful Login" in driver.page_source


    def tearDown(self):
        self.driver.quit()

class Test2(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Remote(command_executor='http://driver:4444/wd/hub', desired_capabilities=DesiredCapabilities.CHROME)

    def test_search_in_python_org(self):
        driver = self.driver

        driver.get('http://thedemosite.co.uk/savedata.php')

        username = driver.find_element_by_name('username')
        username.send_keys("admin")

        password = driver.find_element_by_name('password')
        password.send_keys("admin")

        submit_button = driver.find_element_by_name('FormsButton2')
        submit_button.click()

        driver.get('http://thedemosite.co.uk/login.php')

        username = driver.find_element_by_name('username')
        username.send_keys("admin")

        password = driver.find_element_by_name('password')
        password.send_keys("admin1")

        submit_button = driver.find_element_by_name('FormsButton2')
        submit_button.click()

        assert "Failed Login" in driver.page_source


    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
